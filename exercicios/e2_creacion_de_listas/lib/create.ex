defmodule Create do

#Función crear listas a partir de un numero
# "l" equivale a la lista que vamos a crear
# n es un contador para llevar la cuenta

def create(0) do
  []
end

    def create(i) do
      create(1,[],i)
    end

    def create(i,l,i) do
    (l++[i])
    end

    def create(n,l,i) when i > 0 do
      create((n+1),(l++[n]),i)
    end


#Función crear listas a partir de un numero sentido inverso
# "l" equivale a la lista que vamos a crear
# en esta no hace falta un contador porque el minimo es 0

    def reverse_create(n) do
      reverse_create(n,[])
    end

    def reverse_create(0,l) do
      l
    end

    def reverse_create(n,l) do
      reverse_create(n-1,l++[n])
    end

end
