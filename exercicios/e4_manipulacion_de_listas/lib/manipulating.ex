defmodule Manipulating do

#Funcion de filtro de listas
# r es la lista con el filtro aplicado

  def filter(l,n) do
    filter(n,l,[])
  end

  def filter(_,[],r) do # condicion de parada de la recursividad
    reverse(r)
  end

  def filter(n,[h|t],r) when h<=n do #para cuando no se filtra
    filter(n,t,[h|r])
  end

  def filter(n,[_|t],r) do #cuando se filtra
    filter(n,t,r)
  end

#Funcion dar vuelta a una lista

  def reverse(l) do
     reverse(l,[])
  end

  def reverse([],r) do # Condicion de parada
    r
  end

  def reverse([h|t],r) do
    reverse(t,[h|r])
  end

  #Funcion que une varias listas en una sola.

   def concatenate(l) do #condicion de parada
     concatenate(l,[])
   end

   def concatenate([h|t],r) do
     concatenate(t,auxconc(h,r))
   end

   def concatenate([],r) do
     reverse(r)
   end

  #auxconc para meter cada elemento de la lista en la lista conjunta

   def auxconc([],r) do
     r
   end

   def auxconc([h|t],r) do
     auxconc(t,[h|r])
   end


  def flatten(l) do
    flatten(reverse(l),[])
  end

  def flatten([[]|t],r) do
    flatten(t,r)
  end

  def flatten([],r) do
    r
  end

  def flatten([h|t],r) when is_list(h) do #para las listas de listas
    flatten(t,(flatten(reverse(h),r)))
  end

  def flatten([h|t],r) do
    flatten(t,[h|r])
  end

end
