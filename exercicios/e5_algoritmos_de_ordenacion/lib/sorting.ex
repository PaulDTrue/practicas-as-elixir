defmodule Sorting do

  def quicksort([]) do
      []
  end

  def quicksort([h|t]) do
    {men, may} = Enum.split_with(t, fn a -> (a < h) end)
    quicksort(men) ++ [h] ++ quicksort(may)
  end

  def merge(l1,l2) do
    merge(l1,l2,[])
end


  def merge([],[],r) do
      Enum.reverse r
  end

  def merge([h|t],[],r) do
    merge(t,[],[h|r])
  end

  def merge([],[h|t],r) do
    merge([],t,[h|r])
  end

  def merge([h1|t1],[h2|t2],r) do
    if (h1 > h2) do
      merge([h1|t1],t2,[h2|r])
    else
      merge(t1,[h2|t2],[h1|r])
    end
  end

  def mergesort ([]) do
    []
  end

  def mergesort([l]) do
    [l]
  end

  def mergesort(list) do
      {l, r} = Enum.split(list, div(length(list), 2))
      merge(mergesort(l), mergesort(r))
  end

end
