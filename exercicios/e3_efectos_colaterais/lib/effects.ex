defmodule Effects do

#Escribir enteros entre 1 y N

  def print(n) do
    print(1,n)
  end
# "m" es un contador para ir imprimiendo
  def print(m,n) when m <= n do
    IO.puts(m)
    print(m+1,n)
  end

  def print(_,_) do
    :ok
  end

#Escribir enteros pares entre 1 y N
# rem es para coger el resto de la division

  def even_print(n) do
    even_print(1,n)
  end

  def even_print(m,n) when (rem(m,2)==0) do
    IO.puts(m)
    even_print(m+1,n)
  end

  def even_print(m,n) when m < n do
    even_print(m+1,n)
  end

  def even_print(_,_) do
    :ok
  end

end
