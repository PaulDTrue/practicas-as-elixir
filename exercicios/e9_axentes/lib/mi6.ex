defmodule Mi6 do

  use GenServer
  use Agent

  #Cliente

  def fundar do
    GenServer.start_link(Mi6, Db.new, name: :mi6)
    :ok
  end

  def recrutar(axente ,destino) do
    GenServer.cast(:mi6,{:recruit,axente,destino})
    :ok
  end

  def asignar_mision(axente, mision) do
    GenServer.cast(:mi6,{:assign,axente,mision})
    :ok
  end

  def consultar_estado(axente) do
    GenServer.call(:mi6,{:consult,axente})
  end

  def disolver() do
    GenServer.call(:mi6,{:stop})
    GenServer.stop(:mi6, :normal)
    :ok
  end

  #Server

  @impl true
   def init(db) do
     {:ok, db}
   end

   def start_link(agent,des) do
      Agent.start_link(fn -> Enum.shuffle(Create.create(String.length(des)))  end, name: agent)
    end


   #@impl true
   def handle_cast({:recruit, axente,destino}, db) do
     case (start_link(axente,destino)) do
       {:ok,e} ->
          db = Db.write(db,axente,e)
          {:noreply, db}
       {:error,_} -> {:noreply, db}
     end

   end

   @impl true
   def handle_cast({:check, _,_}, db) do
      IO.inspect db
     {:noreply, db}
   end


   def handle_cast({:assign, axente,:espiar}, db) do
     case Db.read(db,axente) do
       {:ok, ax} -> Agent.update(ax, fn [h|t] -> Manipulating.filter([h|t], h) end)
       _ -> :ok
     end
       {:noreply, db}
   end

   def handle_cast({:assign, axente,:contrainformar}, db) do
     case Db.read(db,axente) do
       {:ok, ax} -> Agent.update(ax, fn lista -> Manipulating.reverse(lista) end)
       _ -> :ok
     end
       {:noreply, db}
   end

   @impl true
   def handle_call({:consult,axente}, _from, db) do
        res = Db.read(db,axente)
        case res do
          {:ok,l} -> {:reply, l|> Agent.get(fn x -> x end),db}
          _ -> {:reply, :you_are_here_we_are_not,db}
        end
   end

   def handle_call({:stop}, _from, db) do
     Enum.map(db, fn {_,e} -> Agent.stop(e) end)
     {:reply,:ok,Db.destroy(db)}
   end
end
