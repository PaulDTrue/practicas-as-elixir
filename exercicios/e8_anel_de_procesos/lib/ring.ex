defmodule Ring do

def start(n,m,msg) do
  first = spawn(fn -> nodo() end) #el primero de todos

  crear(n-1,m,msg,[first],n)
  :ok
end


defp crear(n,m,msg,l,no) do #crea el resto de los pid
  case n do
    0 -> mandar(no,m,msg,Enum.reverse(l))
    _ -> pid = spawn(fn -> nodo() end) #creamos pid
         crear(n-1,m,msg,[pid|l],no)
  end
end


defp mandar(no,m,msg,l) do #envia los pid de la lista
  send hd(l),{[m-1,no,l],msg}
end

def siguiente(_,[_|[]],[h|_]) do
  h
end

def siguiente(pid,[pid|[h|_]],_) do
  h
end

def siguiente(pid,[_|t],l) do
  siguiente(pid,t,l)
end


defp nodo() do
  receive do
    {[0,n,l],_} -> #primer proceso
      send hd(l),{n,l}
      nodo()
    {[m,n,l],msg} -> #los procesos con los mensajes a mostrar
      siguiente = siguiente(self(),l,l)
      IO.puts msg
      send siguiente,{[m-1,n,l],msg}
      nodo()
    {1,_} ->
        Process.exit(self(),:normal)
    {n,l} ->
      siguiente = siguiente(self(),l,l)
      send siguiente, {n-1,l}
      Process.exit(self(),:normal)

  end
end

end
