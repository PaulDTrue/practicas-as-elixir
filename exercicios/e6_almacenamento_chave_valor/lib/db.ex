defmodule Db do

  def new do
     []
  end


#cada elemento de la lista es una tupla key,elemento
# funcion para comprobar si el key esta en la lista
  def comprobar([], _) do
    false
  end

  def comprobar([{k,_}|_], k) do
      true
  end

  def comprobar([{_,_}|t], k) do
      comprobar(t,k)
  end

   def auxW([{k,_}|t],k,e) do #auxiliar para cuando hay que actualizar alguna key
     [{k,e}|t]
   end

   def auxW([{_,_}|t],k,e) do
     auxW(t,k,e)
   end

   def write([],k,e) do
     [{k,e}]
   end

   def write(l,k,e) do
     comprobar = comprobar(l,k)
     case comprobar do
       false -> [{k,e}|l] #introduces una nueva
       true -> auxW(l,k,e) #actualizas una key existente
     end
   end

   def delete(l,k) do
     delete(l,k,[])
   end

   def delete([],_,_) do
     []
   end

    def delete([{k,_}|t],k,r) do
      (r++t)
    end

    def delete([{a,b}|_],_,r) do
       (r++[{a,b}])
    end

   def read([],_) do
     {:error, :not_found}
   end

   def read([{k,b}|_],k) do
     {:ok,b}
   end

   def read([_|t],k) do
     read(t,k)
   end

   def match(l,e) do
     match(l,e,[])
   end

   def match([],_,r) do
     r
   end

   def match([{k,e}|t],e,r) do
     match(t,e,(r++[k]))
   end

   def match([{_,_}|t],e,r) do
     match(t,e,r)
   end

   def destroy(l) do
     destroy(l,[])
   end

   def destroy([],_) do
     {:ok}
   end

   def destroy(l,r) do
     destroy(r,r)
   end
   
end
