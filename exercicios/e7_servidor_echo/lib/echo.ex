defmodule Echo do

  def start do #crea el pid de cada proceso
    pid = spawn(fn -> servidor() end)
    Process.register(pid,:echo)
    :ok
  end

  def servidor do 
    receive do
      {:print,term} ->
          IO.puts term
          servidor()
      :stop -> :ok
    end
  end

  def print(term) do
    send :echo,{:print,term}
    :ok
  end

  def stop do
    send :echo,:stop
    Process.unregister(:echo)
    :ok
  end

end
